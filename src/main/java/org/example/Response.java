package org.example;

import java.time.Duration;

public sealed interface Response permits Response.Failure, Response.RetryAfter, Response.Success {

    record Success(String applicationStatus, String applicationId) implements Response {
    }

    record RetryAfter(Duration delay) implements Response {
    }

    record Failure(Throwable ex) implements Response {
    }
}
