package org.example;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class HandlerImpl implements Handler {

    public static final int TIMEOUT_SEC = 15;
    public static final int MAX_RETRY = 5;
    Client client = new ClientImpl();

    @Override
    public ApplicationStatusResponse performOperation(String id) {
        ApplicationStatusResponse result = null;
        int retryNum = 0;
        long start = 0;
        while (retryNum++ < MAX_RETRY) {
            ExecutorService threadPool = null;
            try {
                threadPool = Executors.newFixedThreadPool(2);
                start = System.currentTimeMillis();
                List<Callable<Response>> callableTasks = new ArrayList<>();
                callableTasks.add(() -> client.getApplicationStatus1(id));
                callableTasks.add(() -> client.getApplicationStatus2(id));
                Response response = threadPool.invokeAny(callableTasks, TIMEOUT_SEC, TimeUnit.SECONDS);
                Response.Success success;
                if (response instanceof Response.Success) {
                    success = (Response.Success) response;
                    return new ApplicationStatusResponse.Success(success.applicationId(), success.applicationStatus());
                } else if (response instanceof Response.RetryAfter retryAfter) {
                    result = new ApplicationStatusResponse.Failure(retryAfter.delay(), retryNum);
                }


            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                System.out.println("retry: " + retryNum + ", error message: " + e.getMessage());
            } finally {
                if (threadPool != null)
                    threadPool.shutdownNow();
            }
        }
        if (result != null)
            return result;
        return new ApplicationStatusResponse.Failure(Duration.of(System.currentTimeMillis() - start, ChronoUnit.MILLIS), retryNum);
    }
}
