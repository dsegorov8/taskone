package org.example;

public class ClientImpl implements Client {

    @Override
    public Response getApplicationStatus1(String id) {
        return new Response.Success("001","APP1");
    }

    @Override
    public Response getApplicationStatus2(String id) {
        return new Response.Success("002","APP2");
    }

}
